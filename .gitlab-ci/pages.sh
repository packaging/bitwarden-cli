#!/bin/bash

set -e

dist=bitwarden-cli

mkdir -p "${CI_PROJECT_DIR}"/public/{deb,rpm}
cp -rv \
  "${CI_PROJECT_DIR}"/.repo/deb/gpg.key \
  "${CI_PROJECT_DIR}"/.repo/deb/"${dist}"/"${dist}"/{dists,pool} \
  "${CI_PROJECT_DIR}"/public/deb/
cp -rv \
  "${CI_PROJECT_DIR}"/.repo/rpm/gpg.key \
  "${CI_PROJECT_DIR}"/.repo/rpm/"${dist}"/* \
  "${CI_PROJECT_DIR}"/public/rpm/
