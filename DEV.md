```
docker run --rm -it -v $PWD:/host -e CI_PROJECT_DIR=/host --tmpfs /tmp:exec -w /tmp --entrypoint bash node:20-bookworm-slim
CI_COMMIT_TAG=cli-v2024.11.1+2 ARCH=arm64 /host/package.sh
```
