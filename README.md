# [Bitwarden CLI](https://bitwarden.com/help/cli/) apt packages

Packages are created using [nfpm](https://nfpm.goreleaser.com/) by pushing released tags and repo is created using [Gitlabs static pages](https://morph027.gitlab.io/blog/repo-hosting-using-gitlab-pages/).

## DEB

### Add repo signing key to apt

```
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-bitwarden-cli.asc https://packaging.gitlab.io/bitwarden-cli/deb/gpg.key
```

### Add repo to apt

```
echo "deb https://packaging.gitlab.io/bitwarden-cli/deb bitwarden-cli main" | sudo tee /etc/apt/sources.list.d/morph027-bitwarden-cli.list
```

### Install

```
sudo apt-get update
sudo apt-get install bitwarden-cli bitwarden-cli-keyring
```

## RPM

### Add repo to yum/dnf

```bash
cat >/etc/yum.repos.d/morph027-bitwarden-cli.repo <<EOF
[bitwarden-cli]
name=morph027-bitwarden-cli
baseurl=https://packaging.gitlab.io/bitwarden-cli/rpm/$basearch
enabled=1
gpgkey=https://packaging.gitlab.io/bitwarden-cli/rpm/gpg.key
gpgcheck=1
repo_gpgcheck=1
EOF
```

### Install

```
sudo dnf install bitwarden-cli
```

## Extras

### unattended-upgrades

To enable automatic upgrades using `unattended-upgrades`, just add the following config file:

```bash
cat > /etc/apt/apt.conf.d/50bitwarden-cli <<EOF
Unattended-Upgrade::Allowed-Origins {
	"morph027:bitwarden-cli";
};
EOF
```
