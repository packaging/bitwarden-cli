#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

script_dir="$(dirname "$(readlink -f "$0")")"
export DEBIAN_FRONTEND=noninteractive
export npm_config_cache="${CI_PROJECT_DIR:-${script_dir}}/.cache/npm"
tag="${CI_COMMIT_TAG:-cli-v2022.11.0+1}"
version="${tag%+*}"
patchlevel="${tag#*+}"
name=bitwarden-cli
description="Bitwarden command-line interface"
url="https://bitwarden.com/help/cli/"
architecture="${ARCH:-amd64}"

function quiet() {
    if [[ -n "${DEBUG}" ]]; then
        # shellcheck disable=SC2048
        $*
    else
        local log
        log="$(mktemp)"
        # shellcheck disable=SC2048
        if ! $* &> "${log}"; then
            cat "${log}"
            exit $?
        fi
    fi
}

function info() {
    printf "\e[1;36m%s\e[0m\n" "$*"
}

function error() {
    printf "\e[1;31m%s\e[0m\n" "$*"
    exit 1
}

function debug() {
    if [[ -n "${DEBUG}" ]]; then
        printf "\e[0;33m%s\e[0m\n" "$@"
    fi
}

case "${architecture}" in
"arm64")
    packages="binfmt-support qemu-user-static git python3 make"
    ;;
esac

info "Installing toolchain ..."
quiet apt-get -qq update
# shellcheck disable=SC2086
quiet apt-get -qqy install \
    curl \
    unzip \
    ${packages}

if ! command -V nfpm >/dev/null 2>&1; then
    nfpm_version="${NFPM_VERSION:-2.41.1}"
    curl -sfLo "${tmpdir}/nfpm.deb" "https://github.com/goreleaser/nfpm/releases/download/v${nfpm_version}/nfpm_${nfpm_version}_${nfpm_architecture:-amd64}.deb"
    apt-get -qqy install "${tmpdir}/nfpm.deb"
fi

tmpdir="$(mktemp -d)"

info "Fetching upstream ..."
file_version=${version##*-}
case "${architecture}" in
"arm64")
    cd "${tmpdir}"
    git clone -b "${version}" --depth 1 https://github.com/bitwarden/clients.git
    cd clients
    npm ci
    cd apps/cli
    sed -i 's#\(^.*"package:oss:lin".*\)#\1\n    "package:oss:lin:arm64": "pkg . --targets linux-arm64 --output ./dist/oss/linux-arm64/bw",#' package.json
    npm run build:oss:prod && npm run clean
    npm run package:oss:lin:arm64
    mv ./dist/oss/linux-arm64/bw "${tmpdir}/"
    cd "${tmpdir}"
    ;;
"amd64")
    curl -Lo "${tmpdir}/cli.zip" "https://github.com/bitwarden/clients/releases/download/${version}/bw-linux-${file_version/v/}.zip"
    unzip -d "${tmpdir}" "${tmpdir}/cli.zip"
    chmod +x "${tmpdir}/bw"
    ;;
esac

info "Creating packages ..."
cat >"${tmpdir}/nfpm.yaml" <<EOF
name: ${name}
arch: ${architecture}
version: ${file_version//v/}+${patchlevel}
version_schema: none
maintainer: "Stefan Heitmüller <stefan.heitmueller@gmx.com>"
description: ${description}
homepage: ${url}
depends:
  - groff
  - less
contents:
  - src: ${tmpdir}/bw
    dst: /usr/bin/
    file_info:
      mode: 0755
EOF
cd "${tmpdir}"
nfpm package --config "${tmpdir}/nfpm.yaml" --packager deb
nfpm package --config "${tmpdir}/nfpm.yaml" --packager rpm
owner="$(stat -c %u "${script_dir}")"
group="$(stat -c %g "${script_dir}")"
install -o "${owner}" -g "${group}" -m 640 ./*"${file_version//v/}+${patchlevel}"*.{deb,rpm} "${script_dir}/"
